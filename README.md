<div align="center">
  <a href="https://gitlab.com/multitech-osp/multi-ui/react" rel="noopener" target="_blank">
    <img width="150" src="./multi-ui.svg" alt="Multi-UI logo">
  </a>
</div>

<div align="center">
  <p>A React library to create consistent user interfaces in a simple way.</p>

[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://github.com/mui/material-ui/blob/HEAD/LICENSE)

</div>

## Commands

```bash
npm start # or yarn start
```

This builds to `/dist` and runs the project in watch mode so any edits you save inside `src` causes a rebuild to `/dist`.

Then run either Storybook or the example playground:

### Storybook

Run inside another terminal:

```bash
yarn storybook
```

This loads the stories from `./stories`.

> NOTE: Stories should reference the components as if using the library, similar to the example playground. This means importing from the root project directory. This has been aliased in the tsconfig and the storybook webpack config as a helper.

To do a one-off build, use `npm run build` or `yarn build`.

To run tests, use `npm test` or `yarn test`.
